<?php 

require "vendor/autoload.php";
use Michelf\Markdown;

$loader = new \Twig\Loader\FilesystemLoader(dirname(__FILE__) . '/views');

$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', '\Twig\Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new \Twig\Extension\DebugExtension()); // Add the debug extension
    
    $twig->addFilter(new \Twig\TwigFilter('markdown', function($string){
        return Markdown::defaultTransform($string);
    }));
});

Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});



Flight::route('/', function(){
    $data = array(
        'dinosaurs' => get_dinosaurs(),
    );
    
    Flight::render('index.twig', $data);
});

Flight::route('/dinosaur/@slug', function($slug){
    $data = array(
        'dino' => get_dinosaur_by_slug($slug),
        'top_rated_dinosaurs' => get_top_rated_dinosaurs(),
    );

    Flight::render('dinosaur.twig', $data);
});


Flight::start();