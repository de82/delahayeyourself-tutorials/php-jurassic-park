<?php

function get_dinosaurs()
{
    $response = Requests::get('https://allosaurus.delahayeyourself.info/api/dinosaurs/');
    if($response->status_code == 200)
    {
        return json_decode($response->body);
    }
    return array();
}

function get_dinosaur_by_slug($slug)
{
    $url = sprintf("https://allosaurus.delahayeyourself.info/api/dinosaurs/%s", $slug);
    $response = Requests::get($url);
    if($response->status_code == 200)
    {
        return json_decode($response->body);
    }
    return null;
}


function get_top_rated_dinosaurs()
{
    $dinosaurs = get_dinosaurs();
    $keys = array_rand($dinosaurs, 3);
    $top = array();
    foreach($keys as $key)
    {
        $top[] = $dinosaurs[$key];
    }
    return $top;
}